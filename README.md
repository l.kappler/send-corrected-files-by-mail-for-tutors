# Send corrected files by mail

The script `send_by_mail.py` allows to automatically send emails containing pdf attachments to the participants of a practice session for which a correction exists.

It has been tested for Linux, Python 3.10.12 and my ...@stud.uni-goettingen.de address. For others some changes might be needed. Feel free to comment the changes you needed to do in an issue to save time for others.

# Setup

You need python with the libraries `smtplib imaplib email configparser csv`. Some or all of them might already be installed by default.

Safe your mail address and password in `send_by_mail.conf` in the same folder as the script.
Format:

```
[server]
mailAddress=... #your mail address
password=ask
timeout=10

[email]
lectureName=StatMech #(will be used in the Subject of the mail.)
messageTemplate=... #(message content template. Use {0} where the Name of the recipient should be inserted. The mail is written in HTML format. Use <br> for new lines.)
```

possibly only "...@stud.uni-goettingen.de" are supported at the moment. You may need to edit the hard coded variable 'MY_USERNAME' to login to the server manually.
If you leave the password as 'ask' it will ask for the password every time.
timeout: the time in sec to wait between each mail that is sent. (To avoid spam detection etc.)

`id_to_student_mapping.csv` contains the table with ids in the first columns, then the recipients name and than the mail address.
You can easily create this file using any spread sheet program to copy the relevant columns from the in studIP downloadable csv file containing all participants.

The corrected files in the Folder containing the current sheets should be named as `{unique_id}_..._korr.pdf` The id should not contain any '_'.
WARNING: If the id is wrong, the file will be sent to the wrong person or not at all. A list is shown of the files and the people receiving the file to check if everything is correct.

If the script fails at some point e.g. due to lost network connection, you can temporarily remove the participants that already received the email and rerun the script.

# Usage

run the script with no arguments using `python send_by_mail.py` enter the address of the folder where the corrections are stored and check if all the files and message contents where sorted correctly. After confirmation and entering the password, The script will send the emails and then write them to your own outgoing mail box. Between each participant the script waits for 10sec or whatever you have set the timeout to. Note that the timeout is not skipped for students who did not hand in and have no correction because otherwise a group of students could deduce wether certain other students handed in just from the timestamps within their own emails.
