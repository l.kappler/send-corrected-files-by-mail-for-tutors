"""
Author: Leo Kappler (and ChatGPT)
Mail: l.kappler@stud.uni-goettingen.de

Script to send corrected files to participants.
Usage:

safe your mail address and password in 'send_by_mail.conf' in the same folder as the script.
Format:

```
[server]
mailAddress=l.kappler@stud.uni-goettingen.de
password=ask
timeout=60

[email]
lectureName=StatMech #(will be used in the Subject of the mail.)
messageTemplate=... #(message content template. Use {0} where the Name of the recipient should be inserted. The mail is written in HTML format. Use <br> for new lines.)
```

possibly only "...@stud.uni-goettingen.de" are supported at the moment. You may need to edit the hard coded variable 'MY_USERNAME' manually.
If you leave the password as 'ask' it will ask for the password every time.
timeout: the time in sec to wait between each mail that is sent. (To avoid spam detection etc.)

id_to_recipient.csv contains the table with ids in the first columns, then the recipients name and than the mail address.

The files in the Folder should be named as {unique_id}_..._korr.pdf The id should not contain any '_'.
WARNING: If the id is wrong, the file will be sent to the wrong person. A list is shown of the files and the people receiving the file to check if everything is correct.

If the script fails at some point e.g. due to lost network connection, you can temporarily remove the participants that already received the email and rerun the script.
"""

import os
import sys
import time

import smtplib
import imaplib
import email
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email import encoders

from configparser import ConfigParser
import csv

config = ConfigParser()
config.read('send_by_mail.conf')

MY_ADDRESS = config.get("server", "mailAddress")
MY_USERNAME = MY_ADDRESS.replace("stud", "student")
PASSWORD = config.get("server", "password")
TIMEOUT = config.getint("server", "timeout")
LECTURE_NAME = config.get("email", "lectureName")
MESSAGE_TEXT = config.get("email", "messageTemplate")

IDS_TO_RECIPIENT = {id_ : (name, address) for id_, name, address in csv.reader(open("id_to_student_mapping.csv", "r"))}


FOLDER = input("which folder contains the corrected PDFs? (with file format '{{id}}..._korr.pdf') ")
os.chdir(FOLDER)

def name(id):
    return IDS_TO_RECIPIENT[id][0]
def email_address(id):
    return IDS_TO_RECIPIENT[id][1]

def send_email_with_pdf(id, filenames, message):
    print(f"sending Email to {name(id)}")
    # Set up the SMTP server
    s = smtplib.SMTP(host='email.stud.uni-goettingen.de', port=587)
    s.starttls()

    # Log in to the server
    s.login(MY_USERNAME, PASSWORD)

    # Create a multipart message
    msg = MIMEMultipart()

    # Set up the parameters of the message
    msg['From'] = MY_ADDRESS
    msg['To'] = email_address(id)
    msg['Subject'] = f"[{LECTURE_NAME}] Zettel Korrektur"

    # Add your message
    msg.attach(MIMEText(message, 'html'))

    # Attach the PDF file
    for file in filenames:
        if not (file.startswith(f"{id}_") and file.endswith('_korr.pdf')):
            if lower(input(f"file {file} does not start with the correct id or doesn't end in _korr.pdf. are you sure? (y/n)")) != "y":
                print(f"skipping {file}.")
                continue

        with open(file, 'rb') as f:
            # Use MIMEApplication instead of MIMEBase
            part = MIMEApplication(f.read(), Name=file)
            # Encode the filename using quoted-printable
            part.add_header('Content-Disposition', 'attachment', filename=email.header.Header(file, 'utf-8').encode())
            msg.attach(part)
    print(str(msg)[:1000],"...")
    time.sleep(1)
    # Send the email
    s.send_message(msg)

    # Terminate the SMTP session
    s.quit()
    print("Message sent. Now writing mail to your own sent folder")

    # Append the email to the 'Sent' folder
    mail = imaplib.IMAP4_SSL("email.stud.uni-goettingen.de")
    mail.login(MY_USERNAME, PASSWORD)
    result, data = mail.append('"Sent"', '(\\Seen)', imaplib.Time2Internaldate(time.time()), str(msg).encode('utf8'))
    if result == 'OK':
        print('Email added to Sent folder')
    else:
        print('Error adding email to Sent folder:')
        print(result, data)
        # response, mailbox_list = mail.list()
        # if response == 'OK':
        #     for mailbox in mailbox_list:
        #         print(mailbox.decode())
        # else:
        #     print('Error listing mailboxes')

    # Close the connection to the IMAP server
    mail.logout()
    print("done")



# find filenames and generate email content.
filenames = {}
message_text = {}
correction_exists = set()
all_files = os.listdir(".")
all_ids = list(IDS_TO_RECIPIENT.keys())
all_ids.sort()
for id in all_ids:
    filenames[id] = []
    for filename in all_files:
        if filename.startswith(f"{id}_") and filename.endswith('_korr.pdf'):
            filenames[id].append(filename)
            correction_exists.add(id)
    message_text[id] = MESSAGE_TEXT.format(name(id))

correction_does_not_exists = set(all_ids).difference(correction_exists)

# print the information gathered and ask for confirmation
print(f"For the students {', '.join([f'{name(id)} ({id})' for id in sorted(correction_does_not_exists)])}, no correction exists.")

print("going to send emails for:")
print("Name, filenames, email content")
for id in all_ids:
    if id in correction_exists:
        print(f"({id}) {name(id):<20} {', '.join(filenames[id]):<50} {message_text[id]}")
    else:
        print(f"({id}) {name(id):<20}   no file found, skipping...")

if input("everything as it should? (y/n)") != "y":
    print("exiting.")
    sys.exit()

# get password and send mails.
if PASSWORD == "ask":
    from getpass import getpass
    PASSWORD = getpass(f"please enter the password for {MY_ADDRESS}: ")

for id in all_ids:
    if id in correction_exists:
        send_email_with_pdf(id, filenames[id], message_text[id])
    else:
        print(f"skipping {name(id)}")
        # wait for timeout even if no mail was send because otherwise a group of students could
        # extract wether specific other students handed in something from only the sent-times of their own corrections.
    time.sleep(TIMEOUT)
